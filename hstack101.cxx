//Vector sum of transverse mass 
#include <vector>
#include <iostream>
#include "TString.h"
#include "TSystemFile.h"
#include "TSystemDirectory.h"
#include "ROOT/RDataFrame.hxx"
#include "THStack.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "stdio.h"
#include <math.h>
#include <Math/Vector4D.h>
#include "TInterpreter.h"	
#include "TSystem.h"		
#include "TFile.h"			
#include "TNtuple.h"		
#include "TText.h"
#include "TLatex.h"			
#include "TFrame.h"

std::vector<TString> getListOfRootFiles(const char *category, const char *dirname = "/home/yyq/a9.7/3D1", const char *ext = ".root")
//选出所需的.root文件
{
    TSystemDirectory dir(dirname, dirname);
    TList *files = dir.GetListOfFiles();
    std::vector<TString> fileList = {};
    if (files)
    {
        TSystemFile *file;
        TString fname;
        TIter next(files);
        while ((file = (TSystemFile *)next()))
        {
            fname = file->GetName();
            if (!file->IsDirectory() && fname.EndsWith(ext))
            {
                 if (0 == strcmp ("bkg",category))
                {
                    if (fname.Contains("bkg"))
                        fileList.push_back(fname);
                }
                else if (0 == strcmp ("sig",category))
                {
                    if (fname.Contains("sig"))
                        fileList.push_back(fname);
                }
                else continue;
            }
        }
    }
    return fileList;
}

Float_t weight; //每个事例的权重
Float_t mass; //Truth mass of Z'
Float_t lep0_pt;
Float_t lep1_pt;
Float_t lep2_pt;
Float_t lep0_eta;
Float_t lep1_eta;
Float_t lep2_eta;
Float_t lep0_phi;
Float_t lep1_phi;
Float_t lep2_phi;
Float_t lep0_E;
Float_t lep1_E;
Float_t lep2_E;
Float_t met_px;
Float_t met_py;
Float_t met_met;
/*
ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
Float_t  et1 = (v0 + v1 + v2).Et();
Float_t  px1 = (v0 + v1 + v2).Px();
Float_t  py1 = (v0 + v1 + v2).Py();
Float_t  pT1 = (px1,py1);
Float_t  pT2 = (met_px,met_py);
Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); //Vector sum of transverse mass 
*/
TH1D *sumbkg(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py));
            h->Fill(mT,weight); 
        }
        sum->Add (h,1);
    }
    return sum;
}
TH1D *sumsig1(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 5)
            {
                h->Fill(mT,weight); 
            }
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig2(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 9)
            {
                h->Fill(mT,weight); 
            }
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig3(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "",30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 15)
            {
                h->Fill(mT,weight); 
            }
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig4(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 19)
            {
                h->Fill(mT,weight); 
            }
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig5(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 23)
            {
                h->Fill(mT,weight); 
            }
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig6(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 27)
            {
                h->Fill(mT,weight); 
            }
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig7(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 31)
            {
                h->Fill(mT,weight); ;
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig8(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 35)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig9(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 39)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig10(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 45)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig11(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 51)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig12(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 54)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig13(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 60)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig14(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 66)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig15(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 69)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig16(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "",30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 75)
            {
                h->Fill(mT,weight); 
            } 
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig17(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            if (mass == 80)
            {
                h->Fill(mT,weight); 
            }  
            else continue;
        }
        sum->Add (h,300);
    }
    return sum;
}
TH1D *sumsig18(std::vector<TString> fileList)
{
    TH1D *sum = new TH1D("sum", "", 30,0,200);
    for (auto file : fileList)
    {
        TString name = file;
        TFile *f = new TFile(file);
        TTree *nominal = (TTree *)f->Get("nominal");
        nominal->SetBranchAddress("weight",&weight);
        nominal->SetBranchAddress("mass",&mass);
        nominal->SetBranchAddress("lep0_pt",&lep0_pt);
        nominal->SetBranchAddress("lep1_pt",&lep1_pt);
        nominal->SetBranchAddress("lep2_pt",&lep2_pt);
        nominal->SetBranchAddress("lep0_eta",&lep0_eta);
        nominal->SetBranchAddress("lep1_eta",&lep1_eta);
        nominal->SetBranchAddress("lep2_eta",&lep2_eta);
        nominal->SetBranchAddress("lep0_phi",&lep0_phi);
        nominal->SetBranchAddress("lep1_phi",&lep1_phi);
        nominal->SetBranchAddress("lep2_phi",&lep2_phi);
        nominal->SetBranchAddress("lep0_E",&lep0_E);
        nominal->SetBranchAddress("lep1_E",&lep1_E);
        nominal->SetBranchAddress("lep2_E",&lep2_E);
        nominal->SetBranchAddress("met_px",&met_px);
        nominal->SetBranchAddress("met_py",&met_py);
        nominal->SetBranchAddress("met_met",&met_met);
        TH1D *h = new TH1D("h","",30,0,200);
        for (int i = 0; i < nominal->GetEntries(); i++)
        {
            nominal->GetEntry(i);
            ROOT::Math::PtEtaPhiEVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_E);
            ROOT::Math::PtEtaPhiEVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_E);
            ROOT::Math::PtEtaPhiEVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_E);
            Float_t  et1 = (v0 + v1 + v2).Et();
            Float_t  px1 = (v0 + v1 + v2).Px();
            Float_t  py1 = (v0 + v1 + v2).Py();
            Float_t  pT1 = (px1,py1);
            Float_t  pT2 = (met_px,met_py);
            Float_t  mT = sqrt((et1 + met_met)*(et1 + met_met) - (px1 + met_px) * (px1 + met_px) - (py1 + met_py) * (py1 + met_py)); 
            h->Fill(mT,weight); 
        }
        sum->Add (h,25);
    }
    return sum;
}

void hstack101() 
{
    ROOT::EnableImplicitMT();
    std::vector<TString> bkgList = getListOfRootFiles("bkg");
    std::vector<TString> sigList = getListOfRootFiles("sig");
    TCanvas *c = new TCanvas("c1", "", 2400, 3600);
    c->Divide(3,6);
    c->cd(1);
    THStack *ts1 = new THStack("hs1", "");
    TH1D *bkg1 = sumbkg(bkgList);
    TH1D *sig1 = sumsig1(sigList);
    bkg1->SetName("bkgName");
    bkg1->SetFillColor(kGreen);
    sig1->SetName("sig1Hist");
    sig1->SetLineColor(kRed);
    sig1->SetLineWidth(1);
    ts1->Add(bkg1);
    ts1->SetMaximum(400);
    ts1->SetMinimum(0);
    TLegend *leg1 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg1->AddEntry(bkg1, "bkg", "f");
    leg1->AddEntry(sig1, "5GeV", "f");
    ts1->Draw("hist");
    sig1->Draw("same hist");
    ts1->GetYaxis()->SetTitle("Events");
    ts1->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg1->Draw("same");
    TText *t11 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t21 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(2);
    THStack *ts2 = new THStack("hs2", "");
    TH1D *bkg2 = sumbkg(bkgList);
    TH1D *sig2 = sumsig2(sigList);
    bkg2->SetName("bkgName");
    bkg2->SetFillColor(kGreen);
    sig2->SetName("sig2Hist");
    sig2->SetLineColor(kRed);
    sig2->SetLineWidth(1);
    ts2->Add(bkg2);
    ts2->SetMaximum(400);
    ts2->SetMinimum(0);
    TLegend *leg2 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg2->AddEntry(bkg2, "bkg", "f");
    leg2->AddEntry(sig2, "9GeV", "f");
    ts2->Draw("hist");
    sig2->Draw("same hist");
    ts2->GetYaxis()->SetTitle("Events");
    ts2->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg2->Draw("same");
    TText *t12 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t22 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(3);
    THStack *ts3 = new THStack("hs3", "");
    TH1D *bkg3 = sumbkg(bkgList);
    TH1D *sig3 = sumsig3(sigList);
    bkg3->SetName("bkgName");
    bkg3->SetFillColor(kGreen);
    sig3->SetName("sig3Hist");
    sig3->SetLineColor(kRed);
    sig3->SetLineWidth(1);
    ts3->Add(bkg3);
    ts3->SetMaximum(400);
    ts3->SetMinimum(0);
    TLegend *leg3 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg3->AddEntry(bkg3, "bkg", "f");
    leg3->AddEntry(sig3, "15GeV", "f");
    ts3->Draw("hist");
    sig3->Draw("same hist");
    ts3->GetYaxis()->SetTitle("Events");
    ts3->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg3->Draw("same");
    TText *t13 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t23 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(4);
    THStack *ts4 = new THStack("hs4", "");
    TH1D *bkg4 = sumbkg(bkgList);
    TH1D *sig4 = sumsig4(sigList);
    bkg4->SetName("bkgName");
    bkg4->SetFillColor(kGreen);
    sig4->SetName("sig4Hist");
    sig4->SetLineColor(kRed);
    sig4->SetLineWidth(1);
    ts4->Add(bkg4);
    ts4->SetMaximum(400);
    ts4->SetMinimum(0);
    TLegend *leg4 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg4->AddEntry(bkg4, "bkg", "f");
    leg4->AddEntry(sig4, "19GeV", "f");
    ts4->Draw("hist");
    sig4->Draw("same hist");
    ts4->GetYaxis()->SetTitle("Events");
    ts4->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg4->Draw("same");
    TText *t14 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t24 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(5);
    THStack *ts5 = new THStack("hs5", "");
    TH1D *bkg5 = sumbkg(bkgList);
    TH1D *sig5 = sumsig5(sigList);
    bkg5->SetName("bkgName");
    bkg5->SetFillColor(kGreen);
    sig5->SetName("sig5Hist");
    sig5->SetLineColor(kRed);
    sig5->SetLineWidth(1);
    ts5->Add(bkg5);
    ts5->SetMaximum(400);
    ts5->SetMinimum(0);
    TLegend *leg5 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg5->AddEntry(bkg5, "bkg", "f");
    leg5->AddEntry(sig5, "23GeV", "f");
    ts5->Draw("hist");
    sig5->Draw("same hist");
    ts5->GetYaxis()->SetTitle("Events");
    ts5->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg5->Draw("same");
    TText *t15 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t25 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(6);
    THStack *ts6 = new THStack("hs6", "");
    TH1D *bkg6 = sumbkg(bkgList);
    TH1D *sig6 = sumsig6(sigList);
    bkg6->SetName("bkgName");
    bkg6->SetFillColor(kGreen);
    sig6->SetName("sig6Hist");
    sig6->SetLineColor(kRed);
    sig6->SetLineWidth(1);
    ts6->Add(bkg6);
    ts6->SetMaximum(400);
    ts6->SetMinimum(0);
    TLegend *leg6 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg6->AddEntry(bkg6, "bkg", "f");
    leg6->AddEntry(sig6, "27GeV", "f");
    ts6->Draw("hist");
    sig6->Draw("same hist");
    ts6->GetYaxis()->SetTitle("Events");
    ts6->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg6->Draw("same");
    TText *t16 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t26 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(7);
    THStack *ts7 = new THStack("hs7", "");
    TH1D *bkg7 = sumbkg(bkgList);
    TH1D *sig7 = sumsig7(sigList);
    bkg7->SetName("bkgName");
    bkg7->SetFillColor(kGreen);
    sig7->SetName("sig7Hist");
    sig7->SetLineColor(kRed);
    sig7->SetLineWidth(1);
    ts7->Add(bkg7);
    ts7->SetMaximum(400);
    ts7->SetMinimum(0);
    TLegend *leg7 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg7->AddEntry(bkg7, "bkg", "f");
    leg7->AddEntry(sig7, "31GeV", "f");
    ts7->Draw("hist");
    sig7->Draw("same hist");
    ts7->GetYaxis()->SetTitle("Events");
    ts7->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg7->Draw("same");
    TText *t17 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t27 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(8);
    THStack *ts8 = new THStack("hs8", "");
    TH1D *bkg8 = sumbkg(bkgList);
    TH1D *sig8 = sumsig8(sigList);
    bkg8->SetName("bkgName");
    bkg8->SetFillColor(kGreen);
    sig8->SetName("sig8Hist");
    sig8->SetLineColor(kRed);
    sig8->SetLineWidth(1);
    ts8->Add(bkg8);
    ts8->SetMaximum(400);
    ts8->SetMinimum(0);
    TLegend *leg8 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg8->AddEntry(bkg8, "bkg", "f");
    leg8->AddEntry(sig8, "35GeV", "f");
    ts8->Draw("hist");
    sig8->Draw("same hist");
    ts8->GetYaxis()->SetTitle("Events");
    ts8->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg8->Draw("same");
    TText *t18 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t28 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(9);
    THStack *ts9 = new THStack("hs9", "");
    TH1D *bkg9 = sumbkg(bkgList);
    TH1D *sig9 = sumsig9(sigList);
    bkg9->SetName("bkgName");
    bkg9->SetFillColor(kGreen);
    sig9->SetName("sig9Hist");
    sig9->SetLineColor(kRed);
    sig9->SetLineWidth(1);
    ts9->Add(bkg9);
    ts9->SetMaximum(400);
    ts9->SetMinimum(0);
    TLegend *leg9 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg9->AddEntry(bkg9, "bkg", "f");
    leg9->AddEntry(sig9, "39GeV", "f");
    ts9->Draw("hist");
    sig9->Draw("same hist");
    ts9->GetYaxis()->SetTitle("Events");
    ts9->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg9->Draw("same");
    TText *t19 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t29 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(10);
    THStack *ts10 = new THStack("hs10", "");
    TH1D *bkg10 = sumbkg(bkgList);
    TH1D *sig10 = sumsig10(sigList);
    bkg10->SetName("bkgName");
    bkg10->SetFillColor(kGreen);
    sig10->SetName("sig1Hist");
    sig10->SetLineColor(kRed);
    sig10->SetLineWidth(1);
    ts10->Add(bkg10);
    ts10->SetMaximum(400);
    ts10->SetMinimum(0);
    TLegend *leg10 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg10->AddEntry(bkg10, "bkg", "f");
    leg10->AddEntry(sig10, "45GeV", "f");
    ts10->Draw("hist");
    sig10->Draw("same hist");
    ts10->GetYaxis()->SetTitle("Events");
    ts10->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg10->Draw("same");
    TText *t110 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t210 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(11);
    THStack *ts11 = new THStack("hs11", "");
    TH1D *bkg11 = sumbkg(bkgList);
    TH1D *sig11 = sumsig11(sigList);
    bkg11->SetName("bkgName");
    bkg11->SetFillColor(kGreen);
    sig11->SetName("sig11Hist");
    sig11->SetLineColor(kRed);
    sig11->SetLineWidth(1);
    ts11->Add(bkg11);
    ts11->SetMaximum(400);
    ts11->SetMinimum(0);
    TLegend *leg11 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg11->AddEntry(bkg11, "bkg", "f");
    leg11->AddEntry(sig11, "51GeV", "f");
    ts11->Draw("hist");
    sig11->Draw("same hist");
    ts11->GetYaxis()->SetTitle("Events");
    ts11->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg11->Draw("same");
    TText *t111 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t211 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(12);
    THStack *ts12 = new THStack("hs12", "");
    TH1D *bkg12 = sumbkg(bkgList);
    TH1D *sig12 = sumsig12(sigList);
    bkg12->SetName("bkgName");
    bkg12->SetFillColor(kGreen);
    sig12->SetName("sig12Hist");
    sig12->SetLineColor(kRed);
    sig12->SetLineWidth(1);
    ts12->Add(bkg12);
    ts12->SetMaximum(400);
    ts12->SetMinimum(0);
    TLegend *leg12 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg12->AddEntry(bkg12, "bkg", "f");
    leg12->AddEntry(sig12, "54GeV", "f");
    ts12->Draw("hist");
    sig12->Draw("same hist");
    ts12->GetYaxis()->SetTitle("Events");
    ts12->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg12->Draw("same");
    TText *t112 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t212 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(13);
    THStack *ts13 = new THStack("hs13", "");
    TH1D *bkg13 = sumbkg(bkgList);
    TH1D *sig13 = sumsig13(sigList);
    bkg13->SetName("bkgName");
    bkg13->SetFillColor(kGreen);
    sig13->SetName("sig13Hist");
    sig13->SetLineColor(kRed);
    sig13->SetLineWidth(1);
    ts13->Add(bkg13);
    ts13->SetMaximum(400);
    ts13->SetMinimum(0);
    TLegend *leg13 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg13->AddEntry(bkg13, "bkg", "f");
    leg13->AddEntry(sig13, "60GeV", "f");
    ts13->Draw("hist");
    sig13->Draw("same hist");
    ts13->GetYaxis()->SetTitle("Events");
    ts13->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg13->Draw("same");
    TText *t113 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t213 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(14);
    THStack *ts14 = new THStack("hs14", "");
    TH1D *bkg14 = sumbkg(bkgList);
    TH1D *sig14 = sumsig14(sigList);
    bkg14->SetName("bkgName");
    bkg14->SetFillColor(kGreen);
    sig14->SetName("sig14Hist");
    sig14->SetLineColor(kRed);
    sig14->SetLineWidth(1);
    ts14->Add(bkg14);
    ts14->SetMaximum(400);
    ts14->SetMinimum(0);
    TLegend *leg14 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg14->AddEntry(bkg14, "bkg", "f");
    leg14->AddEntry(sig14, "66GeV", "f");
    ts14->Draw("hist");
    sig14->Draw("same hist");
    ts14->GetYaxis()->SetTitle("Events");
    ts14->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg14->Draw("same");
    TText *t114 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t214 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(15);
    THStack *ts15 = new THStack("hs15", "");
    TH1D *bkg15 = sumbkg(bkgList);
    TH1D *sig15 = sumsig15(sigList);
    bkg15->SetName("bkgName");
    bkg15->SetFillColor(kGreen);
    sig15->SetName("sig15Hist");
    sig15->SetLineColor(kRed);
    sig15->SetLineWidth(1);
    ts15->Add(bkg15);
    ts15->SetMaximum(400);
    ts15->SetMinimum(0);
    TLegend *leg15 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg15->AddEntry(bkg15, "bkg", "f");
    leg15->AddEntry(sig15, "69GeV", "f");
    ts15->Draw("hist");
    sig15->Draw("same hist");
    ts15->GetYaxis()->SetTitle("Events");
    ts15->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg15->Draw("same");
    TText *t115 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t215 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(16);
    THStack *ts16 = new THStack("hs16", "");
    TH1D *bkg16 = sumbkg(bkgList);
    TH1D *sig16 = sumsig16(sigList);
    bkg16->SetName("bkgName");
    bkg16->SetFillColor(kGreen);
    sig16->SetName("sig16Hist");
    sig16->SetLineColor(kRed);
    sig16->SetLineWidth(1);
    ts16->Add(bkg16);
    ts16->SetMaximum(400);
    ts16->SetMinimum(0);
    TLegend *leg16 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg16->AddEntry(bkg16, "bkg", "f");
    leg16->AddEntry(sig16, "75GeV", "f");
    ts16->Draw("hist");
    sig16->Draw("same hist");
    ts16->GetYaxis()->SetTitle("Events");
    ts16->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg16->Draw("same");
    TText *t116 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t216 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(17);
    THStack *ts17 = new THStack("hs17", "");
    TH1D *bkg17 = sumbkg(bkgList);
    TH1D *sig17 = sumsig17(sigList);
    bkg17->SetName("bkgName");
    bkg17->SetFillColor(kGreen);
    sig17->SetName("sig17Hist");
    sig17->SetLineColor(kRed);
    sig17->SetLineWidth(1);
    ts17->Add(bkg17);
    ts17->SetMaximum(400);
    ts17->SetMinimum(0);
    TLegend *leg17 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg17->AddEntry(bkg17, "bkg", "f");
    leg17->AddEntry(sig17, "80GeV", "f");
    ts17->Draw("hist");
    sig17->Draw("same hist");
    ts17->GetYaxis()->SetTitle("Events");
    ts17->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg17->Draw("same");
    TText *t117 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t217 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c->cd(18);
    THStack *ts18 = new THStack("hs18", "");
    TH1D *bkg18 = sumbkg(bkgList);
    TH1D *sig18 = sumsig18(sigList);
    bkg18->SetName("bkgName");
    bkg18->SetFillColor(kGreen);
    sig18->SetName("sig18Hist");
    sig18->SetLineColor(kRed);
    sig18->SetLineWidth(1);
    ts18->Add(bkg18);
    ts18->SetMaximum(400);
    ts18->SetMinimum(0);
    TLegend *leg18 = new TLegend(0.78, 0.78, 0.88, 0.88);
    leg18->AddEntry(bkg18, "bkg", "f");
    leg18->AddEntry(sig18, "sig", "f");
    ts18->Draw("hist");
    sig18->Draw("same hist");
    ts18->GetYaxis()->SetTitle("Events");
    ts18->GetXaxis()->SetTitle("Vm_{T} [GeV]");
    leg18->Draw("same");
    TText *t118 = new TText(10,355,"ATLAS  Internal");
    t11->SetTextColor(1);
    t11->SetTextSize(0.035);
    t11->SetTextFont(32);
    t11->Draw("same");
    TLatex *t218 = new TLatex(10,330,"#splitline{Low mass Z'}{13 TeV, 139 fb^{-1}}");
    t21->SetTextColor(1);
    t21->SetTextSize(0.03);
    t21->SetTextFont(42);
    t21->Draw("same");
    c-> Print("c101.pdf");
}
                 