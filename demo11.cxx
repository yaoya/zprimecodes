//以m为横坐标轴
#include <vector>
#include <iostream>
#include "TString.h"
#include "TSystemFile.h"
#include "TSystemDirectory.h"
#include "ROOT/RDataFrame.hxx"
#include "THStack.h"
#include "TCanvas.h"
#include "TLegend.h"
#include <iomanip>  //保留两位小数

std::vector<TString> getListOfRootFiles(const char *category, const char *dirname = "/home/yaoyanqi/a9.7/2D1", const char *ext = ".root")
//区分文件
{
    TSystemDirectory dir(dirname, dirname);
    TList *files = dir.GetListOfFiles();
    std::vector<TString> fileList = {};

    if (files)
    {
        TSystemFile *file;
        TString fname;
        TIter next(files);
        while ((file = (TSystemFile *)next()))
        {
            fname = file->GetName();
            if (!file->IsDirectory() && fname.EndsWith(ext))
            {
                if (category == "WZ")
                {
                    if (fname.Contains("364253"))
                        fileList.push_back(fname);
                }
                else if (category == "ZZ")
                {
                    if (fname.Contains("364250"))
                        fileList.push_back(fname);
                }
                else if (category == "Zjets")
                {
                    if (fname.Contains("361106") || fname.Contains("361664") || fname.Contains("361665") || fname.Contains("361107") || fname.Contains("361666") || fname.Contains("361667"))
                        fileList.push_back(fname);
                }
                else if (category == "tt")
                {
                    if (fname.Contains("410472"))
                        fileList.push_back(fname);
                }
                else if (category == "Zgamma")
                {
                    if (fname.Contains("36450"))
                        fileList.push_back(fname);
                }
                else if (category == "five")
                {
                    if (fname.Contains("muvZp005_511340_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "nine")
                {
                    if (fname.Contains("muvZp009_511341_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "one")
                {
                    if (fname.Contains("muvZp015_511342_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "three")
                {
                    if (fname.Contains("muvZp039_511348_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "six")
                {
                    if (fname.Contains("muvZp069_511354_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "seven")
                {
                    if (fname.Contains("muvZp075_511355_mc16"))
                        fileList.push_back(fname);
                }
            }
        }
    }
    return fileList;
}

TH1D *sumHist(std::vector<TString> fileList)
//定义一维直方图函数（累加）
{
    TH1D *sumHist = new TH1D("sumHist", "", 20, 0, 100);
    Float_t crosscutevents = 0;//加了cut之后的事例数乘权重
    //Float_t errsum_w = 0;//误差（权重的平方和开方）
    Float_t varw_i = 0;//权重的平方和
    //Float_t relative_err = 0;//相对误差（reesum_w / varw_i)

    for (auto file : fileList)
    {
        float preweight;
        int getentries;
        float getbincontentone;
        float getbincontenttwo;
        double lumi;
        TString name = file;
        if (name.Contains("16a"))
            lumi = 36.0746;
        else if (name.Contains("16d"))
            lumi = 43.8137;
        else if (name.Contains("16e"))
            lumi = 58.4501;
        TFile *file1 = new TFile(file);
        TDirectoryFile *dir = (TDirectoryFile *)file1->Get("Hist");
        TH1F *dM200 = (TH1F *)dir->Get("hInfo_PFlow");
        getentries = dM200->GetEntries();
        getbincontentone = dM200->GetBinContent(1);
        getbincontenttwo = dM200->GetBinContent(2);
        if (name.Contains("36") || name.Contains("410"))
            preweight = getbincontentone * 2.0 / getentries / getbincontenttwo * lumi;
        else if (name.Contains("muvZp"))
            preweight = 1.0 / getbincontenttwo * lumi;
        
        TTree *tree_PFLOW = (TTree *)file1->Get("tree_PFLOW");
        Float_t weight;
        Float_t lep0_pt;
        Float_t lep1_pt;
        Float_t lep2_pt;
        int n_bjets;
        Float_t lep0_id;
        Float_t lep1_id;
        Float_t lep2_id;
        Float_t lep0_eta;
        Float_t lep1_eta;
        Float_t lep2_eta;
        Float_t lep0_phi;
        Float_t lep1_phi;
        Float_t lep2_phi;
        Float_t lep0_m;
        Float_t lep1_m;
        Float_t lep2_m;
        Float_t lep0_d0sig;
        Float_t lep1_d0sig;
        Float_t lep2_d0sig;
        Int_t lep0_FixedCutPflowTight;
        Int_t lep1_FixedCutPflowTight;
        Int_t lep2_FixedCutPflowTight;

        tree_PFLOW->SetBranchAddress("weight", &weight);
        tree_PFLOW->SetBranchAddress("lep0_pt",&lep0_pt);
        tree_PFLOW->SetBranchAddress("lep1_pt",&lep1_pt);
        tree_PFLOW->SetBranchAddress("lep2_pt",&lep2_pt);
        tree_PFLOW->SetBranchAddress("n_bjets",&n_bjets);
        tree_PFLOW->SetBranchAddress("lep0_id",&lep0_id);
        tree_PFLOW->SetBranchAddress("lep1_id",&lep1_id);
        tree_PFLOW->SetBranchAddress("lep2_id",&lep2_id);
        tree_PFLOW->SetBranchAddress("lep0_eta",&lep0_eta);
        tree_PFLOW->SetBranchAddress("lep1_eta",&lep1_eta);
        tree_PFLOW->SetBranchAddress("lep2_eta",&lep2_eta);
        tree_PFLOW->SetBranchAddress("lep0_phi",&lep0_phi);
        tree_PFLOW->SetBranchAddress("lep1_phi",&lep1_phi);
        tree_PFLOW->SetBranchAddress("lep2_phi",&lep2_phi);
        tree_PFLOW->SetBranchAddress("lep0_m",&lep0_m);
        tree_PFLOW->SetBranchAddress("lep1_m",&lep1_m);
        tree_PFLOW->SetBranchAddress("lep2_m",&lep2_m);
        tree_PFLOW->SetBranchAddress("lep0_d0sig",&lep0_d0sig);
        tree_PFLOW->SetBranchAddress("lep1_d0sig",&lep1_d0sig);
        tree_PFLOW->SetBranchAddress("lep2_d0sig",&lep2_d0sig);
        tree_PFLOW->SetBranchAddress("lep0_FixedCutPflowTight",&lep0_FixedCutPflowTight);
        tree_PFLOW->SetBranchAddress("lep1_FixedCutPflowTight",&lep1_FixedCutPflowTight);
        tree_PFLOW->SetBranchAddress("lep2_FixedCutPflowTight",&lep2_FixedCutPflowTight);
        
        TH1D* Hist_lep1_m=new TH1D("lep1_m",";;",20,0,100);
        for (int i = 0; i < tree_PFLOW->GetEntries(); i++)
        {
            using namespace ROOT::Math;
            tree_PFLOW->GetEntry(i);
            float w;//定义权重
            w =  preweight*weight;
            PtEtaPhiMVector v0(lep0_pt,lep0_eta,lep0_phi,lep0_m);
            PtEtaPhiMVector v1(lep1_pt,lep1_eta,lep1_phi,lep1_m);
            PtEtaPhiMVector v2(lep2_pt,lep2_eta,lep2_phi,lep2_m);
            Float_t mll1; 
            mll1 = (v0 + v1).M();
            Float_t mll2; 
            mll2 = (v0 + v2).M();
            Float_t mll3; 
            mll3 = (v1 + v2).M();
            if (lep0_d0sig < 3 && lep0_d0sig > -3 && lep1_d0sig < 3 && lep1_d0sig > -3 && lep2_d0sig < 3 && lep2_d0sig > -3)
            {
                if (lep0_FixedCutPflowTight == 1 && lep1_FixedCutPflowTight == 1 && lep2_FixedCutPflowTight == 1)
                {
                    if (lep0_id == 13 && lep1_id == 13 && lep2_id == 13)
                    {
                        if (lep0_pt > 20 && lep1_pt > 10 && lep2_pt > 7)
                        {
                            if (n_bjets == 0)
                            {
                                if (mll1 < 80)
                                {
                                    crosscutevents += w;
                                    varw_i += w * w;
                                }
                                else continue;
                            }
                            else continue;
                        }
                        else continue;
                    }
                    else continue;
                }
                else continue;
            }  
            else continue;
            Hist_lep1_m->Fill(mll1,w);
        }
        if (name.Contains("36") || name.Contains("410"))
            sumHist->Add (Hist_lep1_m,1);
        else if (name.Contains("muvZp"))
            sumHist->Add (Hist_lep1_m,200);
    }
    cout.setf(ios::fixed); //setprecision(n)和fixed合用的话可以控制小数点后有几位
    cout << "crosscutevents ± errsum_w ="<< setprecision(2) << crosscutevents;
    cout <<" ± "<< setprecision(2) << sqrt(varw_i)<< endl;
    //cout << "varw_i = "<< setprecision(2) << varw_i << endl;
    //cout << "errsum_w = "<< setprecision(2) << sqrt(varw_i) << endl;
    //cout << "relative_err = "<< setprecision(2) << sqrt(varw_i) / varw_i  << endl;
    return sumHist;
}

void demo11()
{
    ROOT::EnableImplicitMT();

    std::vector<TString> WZList = getListOfRootFiles("WZ");
    std::vector<TString> ZZList = getListOfRootFiles("ZZ");
    std::vector<TString> ZjetsList = getListOfRootFiles("Zjets");
    std::vector<TString> ttList = getListOfRootFiles("tt");
    std::vector<TString> ZgammaList = getListOfRootFiles("Zgamma");
    std::vector<TString> fiveList = getListOfRootFiles("five");
    std::vector<TString> nineList = getListOfRootFiles("nine");
    std::vector<TString> oneList = getListOfRootFiles("one");
    std::vector<TString> threeList = getListOfRootFiles("three");
    std::vector<TString> sixList = getListOfRootFiles("six");
    std::vector<TString> sevenList = getListOfRootFiles("seven");

    THStack *ts = new THStack("hs", "");
    TH1D *WZHist = sumHist(WZList);
    TH1D *ZZHist = sumHist(ZZList);
    TH1D *ZjetsHist = sumHist(ZjetsList);
    TH1D *ttHist = sumHist(ttList);
    TH1D *ZgammaHist = sumHist(ZgammaList);
    TH1D *fiveHist = sumHist(fiveList);
    TH1D *nineHist = sumHist(nineList);
    TH1D *oneHist = sumHist(oneList);
    TH1D *threeHist = sumHist(threeList);
    TH1D *sixHist = sumHist(sixList);
    TH1D *sevenHist = sumHist(sevenList);

    WZHist->SetName("WZName");
    WZHist->SetFillColor(kGreen);
    ZZHist->SetName("ZZName");
    ZZHist->SetFillColor(kBlue);
    ZjetsHist->SetName("ZjetsName");
    ZjetsHist->SetFillColor(kCyan);
    ttHist->SetName("ttName");
    ttHist->SetFillColor(kYellow);
    ZgammaHist->SetName("ZgammaName");
    ZgammaHist->SetFillColor(kCyan - 8);
    fiveHist->SetName("fiveHist");
    fiveHist->SetLineColor(kMagenta);
    fiveHist->SetLineWidth(4);
    nineHist->SetName("nineHist");
    nineHist->SetLineColor(kRed);
    nineHist->SetLineWidth(4);
    oneHist->SetName("oneHist");
    oneHist->SetLineColor(kYellow - 3);
    oneHist->SetLineWidth(4);
    threeHist->SetName("threeHist");
    threeHist->SetLineColor(kGreen + 3);
    threeHist->SetLineWidth(4);
    sixHist->SetName("sixHist");
    sixHist->SetLineColor(kBlue - 9);
    sixHist->SetLineWidth(4);
    sevenHist->SetName("sevenHist");
    sevenHist->SetLineColor(kRed + 3);
    sevenHist->SetLineWidth(4);

    ts->Add(WZHist);
    ts->Add(ZZHist);
    ts->Add(ZjetsHist);
    ts->Add(ttHist);
    ts->Add(ZgammaHist);

    //ts->SetMaximum(500);
    ts->SetMinimum(0);
    TCanvas *c = new TCanvas("c", "", 800, 600);
    c->Draw();
    TLegend *leg = new TLegend(0.7, 0.7, 0.9, 0.9);

    leg->AddEntry(WZHist, "WZ", "f");
    leg->AddEntry(ZZHist, "ZZ", "f");
    leg->AddEntry(ZjetsHist, "Z+jets", "f");
    leg->AddEntry(ttHist, "tt", "f");
    leg->AddEntry(ZgammaHist, "Zgamma", "f");
    leg->AddEntry(fiveHist, "5 GeV", "f");
    leg->AddEntry(nineHist, "9 GeV", "f");
    leg->AddEntry(oneHist, "15 GeV", "f");
    leg->AddEntry(threeHist, "39 GeV", "f");
    leg->AddEntry(sixHist, "69 GeV", "f");
    leg->AddEntry(sevenHist, "75 GeV", "f");

    ts->Draw("hist");
    fiveHist->Draw("same hist");
    nineHist->Draw("same hist");
    oneHist->Draw("same hist");
    threeHist->Draw("same hist");
    sixHist->Draw("same hist");
    sevenHist->Draw("same hist");

    ts->GetYaxis()->SetTitle("Events");
    ts->GetXaxis()->SetTitle("M_{ll,1} [GeV]");
    leg->Draw("same");
}