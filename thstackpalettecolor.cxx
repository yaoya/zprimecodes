#include "TFile.h"


void thstackpalettecolor()
{
     auto hs = new THStack("Z+jets",";P_{T,1} [GeV];Events");
     gStyle->SetPalette(kOcean);

     TFile *f1=new TFile("361106_mc16a.root","read");
     TTree * tree_PFLOW1=(TTree*)f1->Get("tree_PFLOW");
     Float_t lep0_pt;
     tree_PFLOW1->SetBranchAddress("lep0_pt",&lep0_pt);
     // Create 1-d histograms  and add them in the stack
     auto h1st = new TH1F("h1st","361106_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW1->GetEntries();i++)
     {
         tree_PFLOW1->GetEntry(i);
         h1st->Fill(lep0_pt,1);
     }
     hs->Add(h1st);
   
     TFile *f2=new TFile("361106_mc16d.root","read");
     TTree * tree_PFLOW2=(TTree*)f2->Get("tree_PFLOW");
     tree_PFLOW2->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h2st = new TH1F("h2st","361106_mc16d.root",24,0,120);
     for(int i=0;i<tree_PFLOW2->GetEntries();i++)
     {
         tree_PFLOW2->GetEntry(i);
         h2st->Fill(lep0_pt,1);
     }
     hs->Add(h2st);
 
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h5st);
     TFile *f5=new TFile("361107_mc16d.root","read");
     TTree * tree_PFLOW5=(TTree*)f5->Get("tree_PFLOW");
     tree_PFLOW5->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h5st = new TH1F("h5st","361107_mc16d.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW5->GetEntry(i);
         h5st->Fill(lep0_pt,1);
     }
     hs->Add(h5st);
     TFile *f6=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW6=(TTree*)f6->Get("tree_PFLOW");
     tree_PFLOW6->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h6st = new TH1F("h6st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW6->GetEntries();i++)
     { 
         tree_PFLOW6->GetEntry(i);
         h6st->Fill(lep0_pt,1);
     }
     hs->Add(h6st);
     TFile *f7=new TFile("361664_mc16a.root","read");
     TTree * tree_PFLOW7=(TTree*)f7->Get("tree_PFLOW");
     tree_PFLOW7->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h7st = new TH1F("h7st","361664_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW7->GetEntries();i++)
     {
         tree_PFLOW7->GetEntry(i);
         h7st->Fill(lep0_pt,1);
     }
     hs->Add(h7st);
     TFile *f8=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW8=(TTree*)f8->Get("tree_PFLOW");
     tree_PFLOW8->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
     TFile *f3=new TFile("361106_mc16e.root","read");
     TTree * tree_PFLOW3=(TTree*)f3->Get("tree_PFLOW");
     tree_PFLOW3->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h3st = new TH1F("h3st","361106_mc16e.root",24,0,120);
     for(int i=0;i<tree_PFLOW3->GetEntries();i++)
     { 
         tree_PFLOW3->GetEntry(i);
         h3st->Fill(lep0_pt,1);
     }
     hs->Add(h3st);
     TFile *f4=new TFile("361107_mc16a.root","read");
     TTree * tree_PFLOW4=(TTree*)f4->Get("tree_PFLOW");
     tree_PFLOW4->SetBranchAddress("lep0_pt",&lep0_pt);
     auto h4st = new TH1F("h4st","361107_mc16a.root",24,0,120);
     for(int i=0;i<tree_PFLOW4->GetEntries();i++)
     {
         tree_PFLOW4->GetEntry(i);
         h4st->Fill(lep0_pt,1);
     }
     hs->Add(h4st);
 
   // draw the stack
   hs->Draw("pfc nostack");
   gPad->BuildLegend(0.78,0.695,0.980,0.935,"","f");
   hs-> Print(".pdf");
}
