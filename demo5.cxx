//给每一个文件的数据加了权重
#include <vector>
#include <iostream>
#include "TString.h"
#include "TSystemFile.h"
#include "TSystemDirectory.h"
#include "ROOT/RDataFrame.hxx"
#include "THStack.h"
#include "TCanvas.h"
#include "TLegend.h"

std::vector<TString> getListOfRootFiles(const char *category, const char *dirname = "/home/yaoyanqi/a9.7/2D1", const char *ext = ".root")
//区分文件
{
    TSystemDirectory dir(dirname, dirname);
    TList *files = dir.GetListOfFiles();
    std::vector<TString> fileList = {};

    if (files)
    {
        TSystemFile *file;
        TString fname;
        TIter next(files);
        while ((file = (TSystemFile *)next()))
        {
            fname = file->GetName();
            if (!file->IsDirectory() && fname.EndsWith(ext))
            {
                if (category == "WZ")
                {
                    if (fname.Contains("364253"))
                        fileList.push_back(fname);
                }
                else if (category == "ZZ")
                {
                    if (fname.Contains("364250"))
                        fileList.push_back(fname);
                }
                else if (category == "Zjets")
                {
                    if (fname.Contains("361106") || fname.Contains("361664") || fname.Contains("361665") || fname.Contains("361107") || fname.Contains("361666") || fname.Contains("361667"))
                        fileList.push_back(fname);
                }
                else if (category == "tt")
                {
                    if (fname.Contains("410472"))
                        fileList.push_back(fname);
                }
                else if (category == "Zgamma")
                {
                    if (fname.Contains("36450"))
                        fileList.push_back(fname);
                }
                else if (category == "five")
                {
                    if (fname.Contains("muvZp005_511340_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "nine")
                {
                    if (fname.Contains("muvZp009_511341_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "one")
                {
                    if (fname.Contains("muvZp015_511342_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "three")
                {
                    if (fname.Contains("muvZp039_511348_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "six")
                {
                    if (fname.Contains("muvZp069_511354_mc16"))
                        fileList.push_back(fname);
                }
                else if (category == "seven")
                {
                    if (fname.Contains("muvZp075_511355_mc16"))
                        fileList.push_back(fname);
                }
            }
        }
    }
    return fileList;
}

TH1D *sumHist(std::vector<TString> fileList)
//定义一维直方图函数（累加）
{
    TH1D *sumHist = new TH1D("sumHist", "", 24, 0, 120);
    for (auto file : fileList)
    {
        float preweight;
        int getentries;
        float getbincontentone;
        float getbincontenttwo;
        double lumi;
        TString name = file;
        if (name.Contains("16a"))
            lumi = 36.2;
        else if (name.Contains("16d"))
            lumi = 44.3;
        else if (name.Contains("16e"))
            lumi = 58.5;
        TFile *file1 = new TFile(file);
        TDirectoryFile *dir = (TDirectoryFile *)file1->Get("Hist");
        TH1F *dM200 = (TH1F *)dir->Get("hInfo_PFlow");
        getentries = dM200->GetEntries();
        getbincontentone = dM200->GetBinContent(1);
        getbincontenttwo = dM200->GetBinContent(2);
        preweight = getbincontentone * 2.0 / getentries / getbincontenttwo * lumi;
        //cout << "name of dM200:" <<name << endl;
        //cout << "GetEntries of dM200:" << getentries << endl;
        //cout << "GetBinContent(1) of dM200:" << getbincontentone << endl;
        //cout << "GetBinContent(2) of dM200:" << getbincontenttwo << endl;
        //cout << "preweight:" << preweight << endl;
        TTree *tree_PFLOW = (TTree *)file1->Get("tree_PFLOW");
        Float_t weight;
        Float_t lep0_pt;
        tree_PFLOW->SetBranchAddress("weight", &weight);
        tree_PFLOW->SetBranchAddress("lep0_pt",&lep0_pt);
        //TH1D *Hist_weight = new TH1D("weight", ";;", 100, -5000, 7500);
        TH1D* Hist_lep0_pt=new TH1D("lep0_pt",";;",24,0,120);
        for (int i = 0; i < tree_PFLOW->GetEntries(); i++)
        {
            tree_PFLOW->GetEntry(i);
            float a =  preweight*weight;
            Hist_lep0_pt->Fill(lep0_pt, a);
        }
        sumHist->Add(Hist_lep0_pt);
    }
    return sumHist;
}

void demo5()
{
    ROOT::EnableImplicitMT();

    std::vector<TString> WZList = getListOfRootFiles("WZ");
    std::vector<TString> ZZList = getListOfRootFiles("ZZ");
    std::vector<TString> ZjetsList = getListOfRootFiles("Zjets");
    std::vector<TString> ttList = getListOfRootFiles("tt");
    std::vector<TString> ZgammaList = getListOfRootFiles("Zgamma");
    std::vector<TString> fiveList = getListOfRootFiles("five");
    std::vector<TString> nineList = getListOfRootFiles("nine");
    std::vector<TString> oneList = getListOfRootFiles("one");
    std::vector<TString> threeList = getListOfRootFiles("three");
    std::vector<TString> sixList = getListOfRootFiles("six");
    std::vector<TString> sevenList = getListOfRootFiles("seven");

    THStack *ts = new THStack("hs", "");
    TH1D *WZHist = sumHist(WZList);
    TH1D *ZZHist = sumHist(ZZList);
    TH1D *ZjetsHist = sumHist(ZjetsList);
    TH1D *ttHist = sumHist(ttList);
    TH1D *ZgammaHist = sumHist(ZgammaList);
    TH1D *fiveHist = sumHist(fiveList);
    TH1D *nineHist = sumHist(nineList);
    TH1D *oneHist = sumHist(oneList);
    TH1D *threeHist = sumHist(threeList);
    TH1D *sixHist = sumHist(sixList);
    TH1D *sevenHist = sumHist(sevenList);

    WZHist->SetName("WZName");
    WZHist->SetFillColor(kGreen);
    ZZHist->SetName("ZZName");
    ZZHist->SetFillColor(kBlue);
    ZjetsHist->SetName("ZjetsName");
    ZjetsHist->SetFillColor(kCyan);
    ttHist->SetName("ttName");
    ttHist->SetFillColor(kYellow);
    ZgammaHist->SetName("ZgammaName");
    ZgammaHist->SetFillColor(kCyan - 8);
    fiveHist->SetName("fiveHist");
    fiveHist->SetLineColor(kMagenta);
    fiveHist->SetLineWidth(10);
    nineHist->SetName("nineHist");
    nineHist->SetLineColor(kRed);
    nineHist->SetLineWidth(10);
    oneHist->SetName("oneHist");
    oneHist->SetLineColor(kYellow - 3);
    oneHist->SetLineWidth(10);
    threeHist->SetName("threeHist");
    threeHist->SetLineColor(kGreen + 3);
    threeHist->SetLineWidth(10);
    sixHist->SetName("sixHist");
    sixHist->SetLineColor(kBlue - 9);
    sixHist->SetLineWidth(10);
    sevenHist->SetName("sevenHist");
    sevenHist->SetLineColor(kRed + 3);
    sevenHist->SetLineWidth(10);

    ts->Add(WZHist);
    ts->Add(ZZHist);
    ts->Add(ZjetsHist);
    ts->Add(ttHist);
    ts->Add(ZgammaHist);

    ts->SetMaximum(2e7);
    ts->SetMinimum(1e-3);
    TCanvas *c = new TCanvas("c", "", 800, 600);
    c->Draw();
    TLegend *leg = new TLegend(0.7, 0.7, 0.9, 0.9);

    leg->AddEntry(WZHist, "WZ", "f");
    leg->AddEntry(ZZHist, "ZZ", "f");
    leg->AddEntry(ZjetsHist, "Z+jets", "f");
    leg->AddEntry(ttHist, "tt", "f");
    leg->AddEntry(ZgammaHist, "Zgamma", "f");
    leg->AddEntry(fiveHist, "5 GeV", "f");
    leg->AddEntry(nineHist, "9 GeV", "f");
    leg->AddEntry(oneHist, "15 GeV", "f");
    leg->AddEntry(threeHist, "39 GeV", "f");
    leg->AddEntry(sixHist, "69 GeV", "f");
    leg->AddEntry(sevenHist, "75 GeV", "f");

    ts->Draw("hist");
    fiveHist->Draw("same hist");
    nineHist->Draw("same hist");
    oneHist->Draw("same hist");
    threeHist->Draw("same hist");
    sixHist->Draw("same hist");
    sevenHist->Draw("same hist");

    c->SetLogy();
    ts->GetYaxis()->SetTitle("Events");
    ts->GetXaxis()->SetTitle("P_{T,1} [GeV]");
    leg->Draw("same");
    c->SaveAs("aa.pdf");
}