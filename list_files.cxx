#include <vector>
#include <iostream>
#include "TString.h"
#include "TSystemFile.h"
#include "TSystemDirectory.h"

//读取所有文件的.root文件名
void list_files(const char *dirname="/home/yaoyanqi/a9.7/2D1", const char *ext=".root")
{
   TSystemDirectory dir(dirname, dirname);
   TList *files = dir.GetListOfFiles();
   std::vector<TString> signalList = {};
   std::vector<TString> bkgList = {};
   if (files) {
      TSystemFile *file;
      TString fname;
      TIter next(files);
      while ((file=(TSystemFile*)next())) {
         fname = file->GetName();
         if (!file->IsDirectory() && fname.EndsWith(ext)) {
            //cout << fname.Data() << endl;
	   if (fname.Contains("muvZp"))
		{
			signalList.push_back(fname);
		}
      else if (!fname.Contains("data"))
      {
         bkgList.push_back(fname);
      }
           
         }
      }
   }
   std::cout << "The signal files: " << std::endl;
   for (auto i : signalList) std::cout << i << std::endl;
   std::cout << "The background files: " << std::endl;
   for (auto i : bkgList) std::cout << i << std::endl;
}
 
                
