void demo()//把信息从本地的文件中提出来，画图时将纵坐标变成log形式。
{ 
    TFile *f=new TFile("361106_mc16a.root","read");
    TTree * tree_PFLOW=(TTree*)f->Get("tree_PFLOW");
    //cout<< "GetEntries of tree_PFLOW:"<< tree_PFLOW->GetEntries() <<endl;
    // define variable 
    Float_t
    lep0_pt;
    tree_PFLOW->SetBranchAddress("lep0_pt",&lep0_pt);

    TH1F* Hist_lep0_pt=new TH1F("Z+jets",";P_{T,1} [GeV];Events",4,0,120);
    for(int i=0;i<tree_PFLOW->GetEntries();i++)
    {
        tree_PFLOW->GetEntry(i);
        Hist_lep0_pt->Fill(lep0_pt,1);
    }
   
    TCanvas* canvas=new TCanvas("c","c",0,0,800,600);
    canvas->SetLogy();
    canvas->cd();
    Hist_lep0_pt->Draw();
    gPad->BuildLegend(0.78,0.695,0.980,0.935,"","f");
    canvas-> Print(".pdf");
}
